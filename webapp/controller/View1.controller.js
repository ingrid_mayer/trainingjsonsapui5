sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"../model/models",
	"sap/ui/model/Filter",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/Fragment",
	"sap/ui/model/Sorter"

], function (Controller, Models, Filter, JSONModel, FilterOperator, Fragment, Sorter) {
	"use strict";

	return Controller.extend("training.training.controller.View1", {
		oTable: null,
		aVisibleColumns: [],
		_oDialog: null,
		_oPersonalizationDialog: null,
		_oDataInitial: null,
		onInit: function () {
			// Column configuration from models.js
			this.oConfigModel = Models.createColumnConfigModel();
			this.oConfigModel.setDefaultBindingMode("TwoWay");

			// Application table
			this.oTable = this.getView().byId("userTable");
			this.oViewData = {
				activeFilters: [],
				activeSorters: [],
				visibleColumns: this.getVisibleColumns(),
				activeSearchFilter: null
			};
			this.oViewModel = new JSONModel(this.oViewData);
			this.oViewModel.setDefaultBindingMode("TwoWay");
			this.getView().setModel(this.oViewModel);

			// Load json model in table
			this.loadData();

		},

		onAfterRendering: function () {
			var aVisibleColumns = this.oViewData.visibleColumns;
			this._addColumn(this.oTable, aVisibleColumns);
		},

		/**
		 * Loads the data from the JSON file
		 * @public
		 */
		loadData: function () {
			var oModel = new JSONModel("./model/data.json");
			oModel.setDefaultBindingMode("TwoWay");
			this.getView().setModel(oModel, "tableModel");
			this.countItems();

		},

		/**
		 * Gets the columns with the property isVisible set to true
		 * @public
		 * @returns an array with the visible columns
		 */
		getVisibleColumns: function () {
			var aColumns = this.oConfigModel.getData().columns;
			return aColumns.filter(function (oColumn) {
				return oColumn.isVisible === true;
			});
		},

		/**
		 * Gets the new value entered in the search field on live change.
		 * @public
		 */
		onLiveSearch: function (oEvent) {
			var sNewValue = oEvent.getParameters("newValue").newValue;
			this._onSearch(sNewValue);
		},

		/**
		 * Gets the query entered in the search field on enter search.
		 * @public
		 */
		onEnterSearch: function (oEvent) {
			var sQuery = oEvent.getParameters("query").query;
			this._onSearch(sQuery);
		},

		/**
		 * Creates the personalization dialog if it hasn't been created, opens it if it has been created. 
		 * Inside the fnOpen function, a temporaray model is set on the dialog.
		 * the temporary model contains the columns, sorters and filters chosen by the user.
		 * @public
		 */
		onPersonalizationDialogPress: function (oEvent) {
			var that = this;
			var oView = this.getView();
			var fnOpen = function () {
				var aVisibleColumns = that.oViewData.visibleColumns;
				var aActiveFilters = that.oViewData.activeFilters;
				var aActiveSorters = that.oViewData.activeSorters;
				var aTempModel = new JSONModel({
					visibleColumns: aVisibleColumns.map(function (oItem) {
						return jQuery.extend(true, {}, oItem);
					}),
					activeFilters: aActiveFilters.map(function (oItem) {
						return jQuery.extend(true, {}, oItem);
					}),
					activeSorters: aActiveSorters.map(function (oItem) {
						return jQuery.extend(true, {}, oItem);
					}),
					ShowResetEnabled: true
				});
				that._oPersonalizationDialog.setModel(aTempModel);
				that._oPersonalizationDialog.open();
			};
			if (!that._oPersonalizationDialog) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "training.training.fragment.Dialog",
					controller: this
				}).then(function (_oPersonalizationDialog) {
					oView.addDependent(_oPersonalizationDialog);
					that._oPersonalizationDialog = _oPersonalizationDialog;
					fnOpen();
				});
			} else {
				fnOpen();
			}
		},

		/**
		 * Calls three other functions fust dialog customization. Afterwards it closes the personalization dialog.
		 * @public
		 */
		handleOK: function (oEvent) {
			this.getColumnCustomConfiguration();
			this.getSorterCustomConfiguration();
			this.getFilterCustomConfiguration();
			this._oPersonalizationDialog.close();
		},

		/**
		 * Creates the column configuration and sets it to the view model of the controller.
		 * @public
		 */
		getColumnCustomConfiguration: function () {
			var oPanelColumns = this._oPersonalizationDialog.getAggregation("panels")[0];
			var oVisibleColumns = oPanelColumns.getColumnsItems();
			var aColumns = oVisibleColumns.map(function (oVisibleColumn) {
				return {
					"id": oVisibleColumn.getProperty("columnKey"),
					"isVisible": oVisibleColumn.getProperty("visible"),
					"name": oVisibleColumn.getProperty("text")
				};
			});
			this._removeAllColumns();
			this._addColumn(this.oTable, aColumns);
			this.oViewData.visibleColumns = aColumns;
		},

		/**
		 * Creates the sorting configuration and sets it to the view model of the controller.
		 * @public
		 */
		getSorterCustomConfiguration: function () {
			var oBinding = this.oTable.getBinding("rows");
			var counter = this.oTable.getBinding("rows").length;

			var oPanelSorting = this._oPersonalizationDialog.getAggregation("panels")[1];
			var aSorterConditions = oPanelSorting._getConditions();
			var aSorters = aSorterConditions.map(function (oSorterItem) {
				return new Sorter({
					path: oSorterItem.keyField,
					descending: oSorterItem.operation === "Ascending" ? false : true
				});
			});
			oBinding.sort(aSorters);
			this.oViewData.activeSorters = aSorterConditions;
		},

		/**
		 * Creates the filter configuration and sets it to the view model of the controller.
		 * @public
		 */
		getFilterCustomConfiguration: function () {
			var oPanelFilter = this._oPersonalizationDialog.getAggregation("panels")[2];
			var aConditions = oPanelFilter.getConditions();
			this.oViewData.activeFilters = aConditions;
			this.filterData(this.oViewData.activeSearchFilter, this.oViewData.activeFilters);
		},

		/**
		 * Resets the column, sorter and filter configuration and sets it to the view model of the controller.
		 * @public
		 */
		onReset: function (oEvent) {
			this.oViewData = {
				activeFilters: [],
				activeSorters: [],
				visibleColumns: this.getVisibleColumns(),
				activeSearchFilters: []
			};
			this.oViewModel = new JSONModel(this.oViewData);
			this.oViewModel.setDefaultBindingMode("TwoWay");
			this.getView().setModel(this.oViewModel);
			var aTempModel = new JSONModel({
				visibleColumns: this.getVisibleColumns(),
				activeFilters: [],
				activeSorters: [],
				ShowResetEnabled: true
			});
			this._oPersonalizationDialog.setModel(aTempModel);
			this._oPersonalizationDialog.open();
		},

		/**
		 * Closes the dialog configuration.
		 * @public
		 */
		onCancel: function (oEvent) {
			this._oPersonalizationDialog.close();
		},

		/**
		 * Gets the input from the search field and sets it to the view model of the controller.
		 * @public
		 */
		_onSearch: function (sQuery) {
			var filter = null;
			if (sQuery) {
				var aSearchFilters = [];
				var aStringFilters = this.oViewData.visibleColumns;
				aSearchFilters = aStringFilters.map(function (aColumn) {
					return new Filter({
						path: aColumn.id,
						operator: FilterOperator.Contains,
						value1: sQuery
					});
				});
				var oSearchFilter = new Filter({
					filters: aSearchFilters,
					or: true
				});
				filter = oSearchFilter;
			}
			this.oViewData.activeSearchFilter = filter;
			this.filterData(this.oViewData.activeSearchFilter, this.oViewData.activeFilters);
		},

		/**
		 * Combines the search field and dialog Filter. The result is being set to the view model of the controller.
		 * @public
		 */
		filterData: function (searchFilter, dialogFilters) {
			var oBinding = this.oTable.getBinding("rows");
			var aFilters = dialogFilters.map(function (oFilterItem) {
				return new Filter({
					path: oFilterItem.keyField,
					operator: oFilterItem.operation,
					exclude: oFilterItem.exclude,
					value1: oFilterItem.value1
				});
			});
			var aDialogFilter = new Filter({
				filters: aFilters,
				and: true
			});

			var oTotalFilters = new Filter({
				filters: searchFilter ? [aDialogFilter, searchFilter] : [aDialogFilter],
				and: true
			});
			oBinding.filter(oTotalFilters);
			this.countItems();
		},

		countItems: function () {
			var oBinding = this.oTable.getBinding("rows");
			var counter = oBinding.getLength();

			var oModel = this.getView().getModel("tableModel");
			oModel.setProperty("/count", counter);
		},

		/**
		 * Removes columns from the table
		 * @private
		 */
		_removeAllColumns: function () {
			this.oTable.removeAllColumns();
		},

		/**
		 * Adds columns dynamically to the table
		 * @private
		 * @returns {aColumns}, which represents an array of the added Columns (those with the property of isVisible===true)
		 */
		_addColumn: function (oTable, aColumns) {
			// var aVisibleColumns = aColumns.filter(function (oColumn) {
			// 	return oColumn.isVisible;
			// });
			aColumns.forEach(function (col) {
				var oColumn = new sap.ui.table.Column({
					name: col.name,
					label: col.name,
					width: "207px",
					visible: col.isVisible,
					template: new sap.m.Text({
						text: {
							path: 'tableModel>' + col.id
						}
					})
				});
				oTable.addColumn(oColumn);
			});
		}
	});
});